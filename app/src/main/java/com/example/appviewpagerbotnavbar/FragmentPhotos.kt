package com.example.appviewpagerbotnavbar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.appviewpagerbotnavbar.databinding.FragmentPhotosBinding

class FragmentPhotos : Fragment() {

    val args:FragmentPhotosArgs by navArgs()

    private lateinit var binding: FragmentPhotosBinding

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val photo = args.photo

        binding = FragmentPhotosBinding.inflate(layoutInflater, container, false)
        binding.ivCars.setImageResource(photo)

        return binding.root

    }

}
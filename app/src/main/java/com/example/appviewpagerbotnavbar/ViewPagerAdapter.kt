package com.example.appviewpagerbotnavbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.viewpager.widget.PagerAdapter


class myPagerAdapter(context: Context) : PagerAdapter() {
    private val mContext: Context = context
    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val imagesEnum: imagesEnum = imagesEnum.values()[position]
        val inflater = LayoutInflater.from(mContext)
        val layout =
            inflater.inflate(imagesEnum.imageLayout, collection, false) as ViewGroup
        collection.addView(layout)

        layout.setOnClickListener {
            when (layout.id) {
                R.id.layFerrari -> {
                    val action = FragmentViewPagerDirections.actionFragmentViewPagerToFragmentPhotos(R.drawable.bulletproofferrari458specialebyaddarmofrontview)
                    layout.findNavController().navigate(action)
                }
                R.id.layAmg -> {
                    val action = FragmentViewPagerDirections.actionFragmentViewPagerToFragmentPhotos(R.drawable.mercedesamggtbydomanig)
                    layout.findNavController().navigate(action)
                }
                R.id.layCorvette -> {
                    val action = FragmentViewPagerDirections.actionFragmentViewPagerToFragmentPhotos(R.drawable.screenshot20191003at92122am1570109240)
                    layout.findNavController().navigate(action)
                }
            }
        }
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return imagesEnum.values().size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val imagesEnum: imagesEnum = imagesEnum.values()[position]
        return mContext.getString(imagesEnum.imageTitle)
    }

}
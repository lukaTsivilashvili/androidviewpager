package com.example.appviewpagerbotnavbar

enum class imagesEnum(val imageTitle: Int, val imageLayout: Int) {

    FERRARI(R.string.titleFerrari, R.layout.layout_ferrari458),
    AMG(R.string.titleAmg, R.layout.layout_amg_gtr),
    CORVETTE(R.string.titleCorvette, R.layout.layout_corvette);
}
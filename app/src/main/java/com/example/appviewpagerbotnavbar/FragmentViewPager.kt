package com.example.appviewpagerbotnavbar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.appviewpagerbotnavbar.databinding.ActivityMainBinding
import com.example.appviewpagerbotnavbar.databinding.FragmentViewPagerBinding

class FragmentViewPager : Fragment() {

    private lateinit var binding: FragmentViewPagerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentViewPagerBinding.inflate(layoutInflater, container, false)

        initImages()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initImages() {
        binding.viewPager.adapter = myPagerAdapter(requireContext())
    }


}